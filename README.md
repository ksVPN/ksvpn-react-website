# ksVPN - My React Website
This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Slider Puzzle
I started this project to learn React, but found more ways to improve it while learning different parts of web development.

### Current Features
- Fully working slider puzzle
- Customizable source for puzzle image (or video)
    - Paste in link to image
    - Get a random image from a subreddit (uses my own custom API)
    - Upload your own image
- Click counter
- Google Login

### Planned Features
- Puzzle Timer
- High Scores Page
- Full screen image preview to browse subreddit images before selecting
    - Reporting missing images
    - Favorite images
- Refactor older code to better seperate functionality in React Components
    - One big use of this is in the buttons; I've created a more flexible button component, but I don't use it in the older code yet

### Development Skills Learned
Thoughout this project I've refreshed some skills and learned a whole lot more, and am continueing to do so as I finish
- Refreshed knowledge of JavaScript/HTML/CSS
- Learned React
- Created a Custom API to verify and store image links for random image retreieval
    - Learned NodeJS, ExpressJS, Fastify, and SQLite to create the API/DB
    - [Source code for API/DB here](https://glitch.com/edit/#!/faithful-soapy-shrew)
- Continued to learn and develop best practices
