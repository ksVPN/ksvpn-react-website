import './TestComponent.css';
import React from 'react';

function TestComponent() {
    const body = (
        <div id='TestComponent'>
            W: {document.body.clientWidth} H: {document.body.clientHeight}
        </div>
    );
    
    return body;
}

export default TestComponent;