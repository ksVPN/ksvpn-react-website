import React from 'react';

function VideoSource(props) {
    const id = 'tile-R'+props.row+'C'+props.col;
    let xMargin = -props.col*(document.getElementById('app').clientWidth/props.cols);
    let yMargin = -props.row*(document.getElementById('app').clientHeight/props.rows);
    let style = {};

    if (props.row === 0 && props.col === 0) {
        style = {
            width: document.getElementById('app').clientWidth,
            height: document.getElementById('app').clientHeight,
            objectFit: 'cover',
            margin: yMargin + 'px 0 0 ' + xMargin + 'px',
            opacity: 0
        }
    } else {
        style = {
            width: document.getElementById('app').clientWidth,
            height: document.getElementById('app').clientHeight,
            objectFit: 'cover',
            margin: yMargin + 'px 0 0 ' + xMargin + 'px'
        }
    }

    let body;
    if (props.puzzleType === 'image') {
        body = <img className='image' id={id} style={style} src={props.src} alt='A puzzle tile.' />
    } else {
        body = <canvas className='image' id={id} style={style} alt='A puzzle tile.' />
    }

    return body;
}

export default VideoSource;