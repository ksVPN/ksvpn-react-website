import React from 'react';
import TileContainer from './TileContainer';

function PuzzleRow(props) {
    let tiles = [];
    const style = {
        height: (100/props.rows)+'%'
    }

    for (let i=0; i < props.cols; i++) {
        tiles.push(<TileContainer key={'cont-R'+props.row+'C'+i} row={props.row} col={i} rows={props.rows} cols={props.cols} src={props.src} puzzleType={props.puzzleType} videoRef={props.videoRef} onClick={props.onClick}/>)
    }

    return (
        <div id={'R'+ props.row} className='row' style={style}>
            {tiles}
        </div>
    );
}

export default PuzzleRow;